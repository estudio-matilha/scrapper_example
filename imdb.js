const fs      = require('fs');
const request = require('request');
const cheerio = require('cheerio');

var url = 'http://www.imdb.com/title/tt1825683/?ref_=nv_sr_1';

request(url, (err, res, html) => {
  if(!err) {
    const $ = cheerio.load(html);
    var title = $('.title_wrapper h1').text();
    var rating = $('.ratingValue span').text();
    var poster = $('.poster img').attr('src');

    var json = {
      title,
      rating,
      poster
    }

    console.log(json);
  }
});
